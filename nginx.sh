#!/bin/sh

# 准备安装
sudo rm -rf /usr/src/
sudo mkdir -p /usr/src/
sudo chown $USER:$USER /usr/src/

# 询问是否改用LibreSSL
cat << EOF

LibreSSL is a version of the TLS/crypto stack forked from OpenSSL in 2014
LibreSSL是OpenSSL加密软件库的一个分支

EOF

read -p "use LibreSSL ? (Y/n): " ssl
case $ssl in
  y) 
libressl=1 
;;
*) 
libressl=0 
;;
esac 

# 关闭 nginx
sudo systemctl stop nginx
sudo systemctl disable nginx
sudo systemctl unmask nginx.service
sudo systemctl daemon-reload

# 准备
sudo rm -f /usr/sbin/nginx
sudo rm -rf /var/log/nginx/
sudo mkdir -p /var/log/nginx/
sudo mkdir -p /etc/nginx/
sudo mkdir -p /etc/nginx/modules-available
sudo mkdir -p /etc/nginx/modules-enabled
#userdel -r www-data
#useradd -r -g www-data -c "www-data" -d /var/www -s /sbin/nologin nginx
#cat /etc/group
sudo rm -rf /var/lib/nginx
sudo mkdir -p /var/lib/nginx
sudo chown -R root:root /var/lib/nginx
sudo mkdir -p /usr/share/nginx

# 安装依赖
cd
sudo apt update -y
sudo apt install gcc make build-essential logrotate cron -y
sudo apt install libtool git mercurial -y

# 下载 nginx
cd /usr/src/
nginx_v='1.25.2'
wget https://nginx.org/download/nginx-${nginx_v}.tar.gz
tar zxvf ./nginx-${nginx_v}.tar.gz
mv nginx-${nginx_v} nginx
#nginx_v='1.25.2'
#hg clone -b quic https://hg.nginx.org/nginx-quic
#mv nginx-quic nginx

# 下载 openssl
# 开启 https
if [ $libressl -eq 1 ]; then

# 下载 libressl
cd /usr/src/
libressl_v='3.7.3'
wget https://ftp.openbsd.org/pub/OpenBSD/LibreSSL/libressl-${libressl_v}.tar.gz
tar xvzf libressl-${libressl_v}.tar.gz
mv libressl-${libressl_v} libressl

fi

if [ $libressl -eq 0 ]; then

# 下载 quictls
cd /usr/src/
#openssl_v='3.0.9'
#wget https://www.openssl.org/source/openssl-${openssl_v}.tar.gz
#tar -zxvf openssl-${openssl_v}.tar.gz
#mv openssl-${openssl_v} openssl
quictls_v='3.0.9'
wget https://github.com/quictls/openssl/archive/openssl-${quictls_v}-quic1.tar.gz
tar -zxvf openssl-${quictls_v}-quic1.tar.gz
mv openssl-openssl-${quictls_v}-quic1 quictls
#cd /usr/src/quictls
#./Configure --prefix=/usr/src/quictls --openssldir=/usr/src/quictls
#make install_dev
fi


# 下载并安装 zlib 库
# 开启 gzip 压缩
#cd /usr/src/
#git clone https://github.com/cloudflare/zlib.git zlib
# zlib
#make -f Makefile.in distclean
#cd /usr/src/
#zlib_v='1.2.11'
#wget https://zlib.net/zlib-${zlib_v}.tar.gz
#tar -zxvf zlib-${zlib_v}.tar.gz
#mv zlib-${zlib_v} zlib
sudo apt install zlib1g-dev -y

# 下载并安装 pcre 库
# 用于正则表达式
#cd /usr/src/
#pcre_v='8.44'
#wget https://ftp.pcre.org/pub/pcre/pcre-${pcre_v}.tar.gz 
#tar -zxvf pcre-${pcre_v}.tar.gz
#mv pcre-${pcre_v} pcre
sudo apt install libpcre2-dev -y

# 安装 GeoIP2 所需的依赖
# IP查询
#cd /usr/src/
#libmaxminddb_v='1.5.2'
#wget https://github.com/maxmind/libmaxminddb/releases/download/${libmaxminddb_v}/libmaxminddb-${libmaxminddb_v}.tar.gz
#tar -zxvf libmaxminddb-${libmaxminddb_v}.tar.gz
#cd libmaxminddb-${libmaxminddb_v}
#./configure 
#make check && make && make install
#echo /usr/local/lib >> /etc/ld.so.conf.d/local.conf 
#ldconfig
#apt-get install libmaxminddb-dev -y

# 下载 ngx_http_geoip2_module 模块
# IP查询
#mkdir -p /usr/src/nginx-geoip2/
#mkdir -p /usr/local/share/GeoIP/
#cd /usr/src/
#geoip2_v='3.3'
#wget https://github.com/leev/ngx_http_geoip2_module/archive/${geoip2_v}.tar.gz
#tar -zxvf ${geoip2_v}.tar.gz
#mv /usr/src/ngx_http_geoip2_module-${geoip2_v} /usr/src/nginx-geoip2/ngx_http_geoip2_module
 
# 下载 GeoIP2 数据(新)
# IP查询
#cd ~/compile-nginx/conf/src/
#cp GeoLite2-City.mmdb /usr/local/share/GeoIP/GeoLite2-City.mmdb
#cp GeoLite2-Country.mmdb /usr/local/share/GeoIP/GeoLite2-Country.mmdb

# 创建 nginx 全局配置文件
sudo touch /etc/nginx/nginx.conf
sudo tee /etc/nginx/nginx.conf << "EOF"
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;
worker_rlimit_nofile 65535;

events {
        use epoll;
        multi_accept on;
        worker_connections 65535;
}

http {
        charset utf-8;
        sendfile on;
        aio threads;
        #directio 512k;
        tcp_nopush on;
        tcp_nodelay on;
        server_tokens off;
        log_not_found off;
        types_hash_max_size 2048;
        client_max_body_size 16M;

        # MIME
        include mime.types;
        default_type application/octet-stream;

        # Logging
        access_log /var/log/nginx/access.log;
        error_log /var/log/nginx/error.log warn;

        # quic
        quic_gso on;
        quic_retry on;

        include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/sites-enabled/*;
}
EOF

# 创建 nginx 服务进程
sudo touch /lib/systemd/system/nginx.service
sudo tee /lib/systemd/system/nginx.service << "EOF"
[Unit]
Description=A high performance web server and a reverse proxy server
After=network.target nss-lookup.target

[Service]
Type=forking
PIDFile=/run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t -q -g 'daemon on; master_process on;'
ExecStart=/usr/sbin/nginx -g 'daemon on; master_process on;'
ExecReload=/usr/sbin/nginx -g 'daemon on; master_process on;' -s reload
ExecStop=-/sbin/start-stop-daemon --quiet --stop --retry QUIT/5 --pidfile /run/nginx.pid
TimeoutStopSec=5
KillMode=mixed

[Install]
WantedBy=multi-user.target
EOF

# 创建 nginx 日志规则 (自动分割)
sudo touch /etc/logrotate.d/nginx
sudo tee /etc/logrotate.d/nginx << "EOF"
/var/log/nginx/*.log {
  daily
  missingok
  rotate 14
  delaycompress
  notifempty
  create 640 www-data adm
  sharedscripts
  postrotate
      /bin/kill -USR1 `cat /var/run/nginx.pid 2>/dev/null` 2>/dev/null || true
  endscript
}
EOF

ldconfig

# 设置默认的网站
sudo mkdir -p /etc/nginx/sites-available
sudo mkdir -p /etc/nginx/sites-enabled
sudo mkdir -p /var/www/html
sudo touch /var/www/html/index.html
sudo tee /var/www/html/index.html << "EOF"
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
EOF

sudo touch /etc/nginx/sites-available/default.conf
sudo tee /etc/nginx/sites-available/default.conf << "EOF"
server {
        listen 80;
        listen [::]:80;
        root /var/www/html;
        index index.html index.htm;
        location / {
                try_files $uri $uri/ =404;
        }
}
EOF
sudo ln -s /etc/nginx/sites-available/default.conf /etc/nginx/sites-enabled/default


# 为网站所在的目录设置权限
sudo chown root:root /var/www/
find /var/www/ -type d -exec sudo chmod 755 {} \;
find /var/www/ -type f -exec sudo chmod 644 {} \;
#chown -R root:www-data /var/www/
#find /var/www/ -type d -exec chmod 750 {} \;
#find /var/www/ -type f -exec chmod 640 {} \;


# 半自动编译
# 从 libressl 和 quictls 之间选一个
if [ $libressl -eq 1 ]; then
    libssl='libressl'
fi

if [ $libressl -eq 0 ]; then
    libssl='quictls'
fi

# 关闭 nginx 的 debug 模式
#sed -i 's@CFLAGS="$CFLAGS -g"@#CFLAGS="$CFLAGS -g"@' /usr/src/nginx/auto/cc/gcc

# 编译安装 nginx
cd /usr/src/nginx

CONFIG="\
--prefix=/usr/share/nginx \
--sbin-path=/usr/sbin/nginx \
--conf-path=/etc/nginx/nginx.conf \
--http-log-path=/var/log/nginx/access.log \
--error-log-path=/var/log/nginx/error.log \
--lock-path=/var/lock/nginx.lock \
--pid-path=/run/nginx.pid \
--modules-path=/usr/lib/nginx/modules \
--http-client-body-temp-path=/var/lib/nginx/body \
--http-fastcgi-temp-path=/var/lib/nginx/fastcgi \
--http-proxy-temp-path=/var/lib/nginx/proxy \
--http-scgi-temp-path=/var/lib/nginx/scgi \
--http-uwsgi-temp-path=/var/lib/nginx/uwsgi \
--with-http_ssl_module \
--with-http_realip_module \
--with-http_gzip_static_module \
--with-http_gunzip_module \
--with-http_auth_request_module \
--with-http_slice_module \
--with-pcre \
--with-pcre-jit \
--with-http_addition_module \
--with-http_sub_module \
--with-compat \
--with-threads \
--with-file-aio \
--with-http_v2_module \
--with-http_v3_module \
--with-openssl=/usr/src/quictls \
"

./auto/configure $CONFIG

sleep 30

echo "ready?"

make

sudo make install

sudo ldconfig

# 将 geoip2 模块复制到标准目录
#cd objs
#sudo cp *.so /usr/lib/nginx/modules
#sduo cp ngx_http_geoip2_module.so /usr/lib/nginx/modules

# 开启 nginx 服务进程
sudo systemctl unmask nginx.service
sudo systemctl daemon-reload
sudo systemctl enable nginx
sudo systemctl start nginx

# 卸载不必要的软件
sudo apt purge gcc make build-essential zlib1g-dev libpcre2-dev libpcre3-dev libmaxminddb-dev libtool mercurial -y
sudo apt autoremove -y

# 测试 nginx
sudo nginx -t

# 修改日志文件的权限
sudo chmod 755 /var/log/nginx/
sudo chown -R www-data:adm /var/log/nginx/
#sudo chown root:root /var/log/nginx/
sudo chmod 640 /var/log/nginx/*
