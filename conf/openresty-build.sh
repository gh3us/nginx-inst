#!/bin/bash

# 下载 openresty
cd /usr/src/
version='1.19.3.1'
wget https://openresty.org/download/openresty-${version}.tar.gz
tar zxvf openresty-${version}.tar.gz
mv openresty-${version} openresty

# 编译安装 openresty
#apt-get install libpcre3-dev libssl-dev perl zlib1g-dev -y
#mkdir -p /usr/local/nginx/nginx/modules
#cd openresty
#./configure --prefix=/usr/local/nginx --sbin-path=/usr/sbin/nginx --pid-path=/var/run/nginx.pid --user=nginx --group=nginx --modules-path=/usr/local/nginx/nginx/modules --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --lock-path=/var/lock/nginx.lock \
#--without-http_memc_module --without-http_redis_module --without-http_redis2_module --without-mail_pop3_module --without-mail_imap_module --without-mail_smtp_module --without-http_rds_json_module --without-http_rds_csv_module --without-http_uwsgi_module --without-http_scgi_module --without-http_autoindex_module --without-http_memcached_module --without-http_empty_gif_module --without-lua_resty_memcached --without-lua_resty_mysql \
#--with-compat --with-file-aio --with-threads --with-luajit --with-http_realip_module --with-http_slice_module --with-http_v2_module --with-http_stub_status_module --with-openssl=/usr/src/openssl-1.1.1g --with-http_ssl_module --with-pcre=/usr/src/pcre-8.44 --with-pcre --with-pcre-jit --with-zlib=/usr/src/zlib --with-http_gzip_static_module 

#make 

#make install

#ldconfig

# 下载 ModSecurity-nginx 模块
cd /usr/src/
git clone --depth 1 https://github.com/SpiderLabs/ModSecurity-nginx.git

# 编译 openresty 并安装 ModSecurity-nginx 模块
cd /usr/src/openresty
mkdir -p /usr/local/nginx/nginx/modules
mkdir -p /var/tmp/nginx/
./configure --prefix=/usr/local/nginx --sbin-path=/usr/sbin/nginx --pid-path=/var/run/nginx.pid --user=nginx --group=nginx --modules-path=/usr/local/nginx/nginx/modules --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --lock-path=/var/lock/nginx.lock \
--http-proxy-temp-path=/var/tmp/nginx/proxy --http-fastcgi-temp-path=/var/tmp/nginx/fastcgi --http-uwsgi-temp-path=/var/tmp/nginx/uwsgi --http-scgi-temp-path=/var/tmp/nginx/scgi \
--without-http_memc_module --without-http_redis_module --without-http_redis2_module --without-mail_pop3_module --without-mail_imap_module --without-mail_smtp_module --without-http_rds_json_module --without-http_rds_csv_module --without-http_uwsgi_module --without-http_scgi_module --without-http_autoindex_module --without-http_memcached_module --without-http_empty_gif_module --without-lua_resty_memcached --without-lua_resty_mysql \
--with-compat --with-file-aio --with-threads --with-luajit --with-http_realip_module --with-http_slice_module --with-http_v2_module --with-http_stub_status_module --with-openssl=/usr/src/openssl --with-http_ssl_module --with-pcre=/usr/src/pcre --with-pcre --with-pcre-jit --with-zlib=/usr/src/zlib --with-http_gzip_static_module \
--add-dynamic-module=/usr/src/ModSecurity-nginx


make 

make install

ldconfig

# 复制 ModSecurity-nginx 模块到标准目录
#cd objs
#cp ngx_http_modsecurity_module.so /usr/local/nginx/nginx/modules


# ModSecurity 初始化设置
cd /usr/src/
mkdir -p /usr/local/nginx/nginx/conf/waf

wget https://example.com/conf/src/modsecurity.conf
cp -p /usr/src/modsecurity.conf /usr/local/nginx/nginx/conf/waf/modsecurity.conf

cd /usr/src/
wget https://github.com/SpiderLabs/ModSecurity/releases/download/v3.0.4/modsecurity-v3.0.4.tar.gz
tar -xvf modsecurity-v3.0.4.tar.gz
cp -p /usr/src/modsecurity-v3.0.4/unicode.mapping /usr/local/nginx/nginx/conf/waf/unicode.mapping

cd /usr/src/
wget https://example.com/conf/src/owasp_waf.conf
cp -p /usr/src/owasp_waf.conf /usr/local/nginx/nginx/conf/waf/owasp_waf.conf


# 下载 OWASP 规则
cd /usr/src/
wget https://github.com/coreruleset/coreruleset/archive/v3.3.0.tar.gz -O CRS_v3.3.0.tar.gz
tar -xvf /usr/src/CRS_v3.3.0.tar.gz
mv coreruleset-3.3.0 crs
cp -r /usr/src/crs /usr/local/nginx/nginx/conf/waf/crs
cp -p /usr/src/crs/crs-setup.conf.example /usr/local/nginx/nginx/conf/waf/crs/crs-setup.conf
sed -i 's?SecDefaultAction \"phase:1,log,auditlog,pass\"?# SecDefaultAction \"phase:1,log,auditlog,pass\"?g' /usr/local/nginx/nginx/conf/waf/crs/crs-setup.conf
sed -i 's?SecDefaultAction \"phase:2,log,auditlog,pass\"?# SecDefaultAction \"phase:2,log,auditlog,pass\"?g' /usr/local/nginx/nginx/conf/waf/crs/crs-setup.conf
sed -i 's?# SecDefaultAction \"phase:1,log,auditlog,deny,status:403\"?SecDefaultAction "phase:1,log,auditlog,deny,status:403"?g' /usr/local/nginx/nginx/conf/waf/crs/crs-setup.conf
sed -i 's?# SecDefaultAction \"phase:2,log,auditlog,deny,status:403\"?SecDefaultAction "phase:2,log,auditlog,deny,status:403"?g' /usr/local/nginx/nginx/conf/waf/crs/crs-setup.conf


# 创建 nginx 全局配置文件
cd /usr/src/
wget https://example.com/conf/src/nginx.conf
rm -f /usr/local/nginx/nginx/conf/nginx.conf
cp -p /usr/src/nginx.conf /usr/local/nginx/nginx/conf/nginx.conf 


# 创建 nginx 服务进程
mkdir -p /usr/lib/systemd/system/
cd /usr/src/
wget https://example.com/conf/src/nginx.service
cp -p /usr/src/nginx.service /usr/lib/systemd/system/nginx.service

# 创建 nginx 日志规则 (自动分割)
cat /dev/null > /etc/logrotate.d/nginx
cat > /etc/logrotate.d/nginx << "EOF"
/var/log/nginx/*.log {
  daily
  missingok
  rotate 24
  delaycompress
  notifempty
  create 640 nginx nginx
  sharedscripts
  postrotate
      /bin/kill -USR1 `cat /var/run/nginx.pid 2>/dev/null` 2>/dev/null || true
  endscript
}
EOF
ldconfig

# 配置默认站点
mkdir -p /wwwroot/
cp -r /usr/local/nginx/nginx/html /wwwroot/default
mkdir -p /usr/local/nginx/nginx/conf/vhost/
mkdir -p /usr/local/nginx/nginx/conf/ssl/
chown -R root:root /usr/local/nginx/nginx/conf/vhost/
chown -R root:root /usr/local/nginx/nginx/conf/ssl/
find /usr/local/nginx/nginx/conf/ssl/ -type d -exec chmod 755 {} \;
cat /dev/null > /usr/local/nginx/nginx/conf/vhost/default.conf
cat > "/usr/local/nginx/nginx/conf/vhost/default.conf" << "EOF"
server {
  listen 80;
  root /wwwroot/default;
  location / {
    index  index.html;
  }
}
EOF

# 创建虚拟主机
cd /usr/src/
wget https://example.com/conf/src/test.conf
cp -p /usr/src/test.conf /usr/local/nginx/nginx/conf/vhost/test.conf 


# 配置站点目录权限
chown -R nginx:nginx /wwwroot/default/
find /wwwroot/ -type d -exec chmod 755 {} \;
find /wwwroot/ -type f -exec chmod 644 {} \;


# 开启 nginx 服务进程
systemctl unmask nginx.service
systemctl daemon-reload
systemctl enable nginx
systemctl start nginx
