#!/bin/bash

cd /usr/src/

# 读取temp.txt文件中的数值，将数值赋值到变量中
libressl=$(cat /usr/src/temp.txt | grep libressl | tr -cd "[0-9]")
echo $libressl

# 从 libressl 和 quictls 之间选一个
if [ $libressl -eq 1 ]; then

libssl='libressl'

fi

if [ $libressl -eq 0 ]; then

libssl='quictls'

fi

# 关闭 nginx 的 debug 模式
#sed -i 's@CFLAGS="$CFLAGS -g"@#CFLAGS="$CFLAGS -g"@' /usr/src/nginx/auto/cc/gcc

# 编译安装 nginx
cd /usr/src/nginx

CONFIG="\
--prefix=/etc/nginx \
--sbin-path=/usr/sbin/nginx \
--conf-path=/etc/nginx/nginx.conf \
--http-log-path=/var/log/nginx/access.log \
--error-log-path=/var/log/nginx/error.log \
--lock-path=/var/lock/nginx.lock \
--pid-path=/run/nginx.pid \
--modules-path=/usr/lib/nginx/modules \
--http-client-body-temp-path=/var/lib/nginx/body \
--http-fastcgi-temp-path=/var/lib/nginx/fastcgi \
--http-proxy-temp-path=/var/lib/nginx/proxy \
--http-scgi-temp-path=/var/lib/nginx/scgi \
--http-uwsgi-temp-path=/var/lib/nginx/uwsgi \
--with-http_ssl_module \
--with-http_realip_module \
--with-http_gzip_static_module \
--with-http_gunzip_module \
--with-http_auth_request_module \
--with-http_slice_module \
--with-pcre \
--with-pcre-jit \
--with-http_addition_module \
--with-http_sub_module \
--with-compat \
--with-threads \
--with-file-aio \
--with-http_v2_module \
--with-http_v3_module \
--with-openssl=/usr/src/quictls \
"

./auto/configure $CONFIG \
--with-cc-opt="-I /usr/src/quictls/include" \
--with-ld-opt="-L /usr/src/quictls/lib64"

sleep 30

echo "ready?"

make


make install

ldconfig

# 将 geoip2 模块复制到标准目录
#cd objs
#cp *.so /usr/lib/nginx/modules
#cp ngx_http_geoip2_module.so /usr/lib/nginx/modules

# 开启 nginx 服务进程
systemctl unmask nginx.service
systemctl daemon-reload
systemctl enable nginx
systemctl start nginx

# 卸载不必要的软件
apt purge gcc make build-essential zlib1g-dev libpcre2-dev libpcre3-dev libmaxminddb-dev libtool mercurial -y
apt autoremove -y

# 测试 nginx
nginx -t

# 修改日志文件的权限
chmod 755 /var/log/nginx/
chown -R www-data:adm /var/log/nginx/
#chown root:root /var/log/nginx/
chmod 640 /var/log/nginx/*

