#!/bin/sh

#set -o errexit

#
root_need() {
    if [ `whoami` != "root" ]; then
        echo "Error:This script must be run as root!" 1>&2
        exit 1
    fi
}

# 检查密码复杂度的函数
CheckPassword()
{

# 检查密码是否过于简单
if echo "$password" | cracklib-check | grep -q 'OK'; then 
    echo "ok"
    echo "The password is $password"
else
    echo "bad"
    echo "The password is too simple"
    exit 1
fi

}


# 产生随机密码的函数
MakePassword()
{

# 自动产生14位的随机密码
#password="$(cat /dev/random | tr -dc '[:graph:]' | head -c 14)"
password="$(cat /dev/urandom | tr -dc 'A-Za-z0-9!#%\&\()*+,./:;<=>?@[\]^_`{|}~' | head -c 14)"

# 产生加密的密码
if [ $stable -eq 1 ]; then
    apt-get install whois -y
    EncryptedPassword=$(mkpasswd --method=yescrypt $password)
else
    EncryptedPassword=$(python3 -c 'import crypt, sys; print(crypt.crypt(str(sys.argv[1]), crypt.mksalt(crypt.METHOD_SHA512)))' $password)
fi

echo $EncryptedPassword

}

# 检查当前用户是否有root权限
root_need

# 询问是否改用stable分支
cat << EOF

The current "stable" distribution of Debian is version 12, codenamed bookworm.
当前 Debian 的"稳定版"版本号是 12，开发代号为 bookworm

EOF

read -p "use Debian stable ? 使用 Debian 稳定版 ？(Y/n): " input
case $input in
  n)
stable=0
;;
*)
stable=1
;;
esac


# 询问是否创建swap交换分区 
cat << EOF

Similar to Pagefile.sys，Swapfile is a Linux feature that takes advantage of space on your hard drive when your RAM either fills up or can be used in a more efficient way.
类似于 Windows 上的分页文件, Swapfile 是 Linux 的一个功能，当你的内存满了或许可以更有效地利用你的硬盘空间。

EOF

read -p "create swapfile ? 是否创建新的 Swap 文件？(Y/n): " input
case $input in
  n)
swap=0
swap_size=''
;;
*)
swap=1
swap_size='1G'
;;
esac


# 询问OpenSSH的端口号
cat << EOF

Changing default port in OpenSSH.
更改 OpenSSH 的默认端口。

EOF

read -p "Type the port number . 请输入端口号 。(1024-65535): " openssh_port
until
    [ $openssh_port -gt 1024 -a $openssh_port -lt 65535 ]
do
    echo "Invalid input,try again. 输入错误，请重试 "
    read -p "Please type a number between 1024 to 65535 请输入1024到65535之间的数字: " openssh_port
done

echo "OpenSSH server are configured to use port $openssh_port."

echo "OpenSSH 服务端程序即将改用$openssh_port端口 "

# 询问OpenSSH是否使用IPv6网络
cat << EOF

Is OpenSSH server allowed to use IPv6? ?
OpenSSH 服务端程序是否使用 IPv6 网络？

EOF

read -p "OpenSSH use IPv6 ? OpenSSH 使用 IPv6 ？(Y/n): " input
case $input in
  n)
openssh_ipv6=0
;;
*)
openssh_ipv6=1
;;
esac

# 询问是否限制OpenSSH的通信范围
cat << EOF

Is OpenSSH server only used for intranet?
OpenSSH 服务端吗只跟私有网络进行通讯吗？

EOF

read -p "private network only ? 仅限私有网络 ？(Y/n): " input
case $input in
  y)
vpc=1
;;
*)
vpc=0
;;
esac


# 询问是否安装mosh
cat << EOF

Mosh is a replacement for interactive SSH terminals.
Mosh是交互式SSH终端的替代品

EOF

read -p "install Mosh ? 安装 Mosh ？(Y/n): " input
case $input in
  y)
mosh=1
;;
*)
mosh=0
;;
esac

# 询问是否激活自动更新
cat << EOF

Automatically download and install stable updates?
想自动下载更新吗？想自动安装更新吗？

EOF

read -p "enable autoupdate ? 需要自动更新吗 ？(Y/n): " input
case $input in
  y)
autoupdate=1
;;
*)
autoupdate=0
;;
esac

# 询问是否开启APT的安全选项
cat << EOF

APT has supported optional seccomp-bpf filtering. This restricts the syscalls that APT is allowed to execute, which can severely limit an attacker's ability to do harm to the system if they attempt to exploit a vulnerability in APT. 
APT可以启用seccomp-bpf过滤。这限制了系统调用，能阻止攻击者利用APT中的漏洞

EOF

read -p "enable seccomp-bpf filtering ? 启用seccomp-bpf过滤吗 ？(Y/n): " input
case $input in
  y)
seccomp=1
;;
*)
seccomp=0
;;
esac

# 询问是否自动锁定账号
cat << EOF

pam_faillock - locks the account in case there were more than deny consecutive failed authentications.
如果输错密码次数过多，则自动锁定账号

EOF

read -p "use pam_faillock ? 自动锁定账号吗 ？(Y/n): " input
case $input in
  n)
faillock=0
;;
*)
faillock=1
;;
esac

# 询问APT是不是只使用IPv6网络
cat << EOF

Does APT use only IPv6 networks ?
APT 下载更新时只使用 IPv6 网络？

EOF

read -p " APT only use IPv6 ? APT 只使用 IPv6 ？(Y/n): " input
case $input in
  y)
apt_ipv6=1
;;
*)
apt_ipv6=0
;;
esac

# 打开APT的安全选项
if [ $seccomp -eq 1 ]; then

    touch /etc/apt/apt.conf.d/40sandbox
    echo 'APT::Sandbox::Seccomp "true";' | tee -a /etc/apt/apt.conf.d/40sandbox

fi

# APT只使用IPv6网络，APT不使用IPv4网络
if [ $apt_ipv6 -eq 1 ]; then

    touch /etc/apt/apt.conf.d/1000-force-ipv6-transport
    echo 'Acquire::ForceIPv6 "true";' | tee -a /etc/apt/apt.conf.d/1000-force-ipv6-transport

fi

# 调整swap空间
if [ $swap -eq 1 ]; then

    mkdir -p /swap
    fallocate -l ${swap_size} /swap/swapfile
    chmod 600 /swap/swapfile
    mkswap /swap/swapfile
    swapon /swap/swapfile
    cp /etc/fstab /etc/fstab.bak
    #chattr +i /etc/fstab.bak
    echo '/swap/swapfile none swap defaults 0 0' | tee -a /etc/fstab

fi

# 检查有没有安装CA根证书
if [ "$(whereis ca-certificates)" = ca-certificates":" ]; then
    echo "ca-certificates is not installed!"
    apt-get clean
    apt-get update
    apt-get install ca-certificates -y
else
    echo "ca-certificates is installed."
fi

# 询问将来想用哪一个镜像源
cat << EOF

Using a nearby server will probably speed up your download. The default mirror is ftp.debian.org
选择离你更近的镜像源，可以加快软件的下载速度。默认的镜像源是ftp.debian.org

EOF

cat << EOF

Everything you want to know about Debian mirrors: https://www.debian.org/mirror/list
关于Debian镜像源的资讯可以在此获得：https://www.debian.org/mirror/list

EOF

sure="undefined"

read -p "Please enter the domain of the mirror 请输入镜像源的域名 [default=ftp.debian.org] : " domian
mirror=${domian:-"ftp.debian.org"}
sed -i "s?ftp.debian.orgS?$mirror?g" /etc/apt/sources.list
printf "############################################################# \n"
printf "The domain of the mirror is: $mirror \n"
printf "你所选择的镜像源是：$mirror \n"
printf "############################################################# \n"
read -p "Are you sure? 你确定吗？(y/n) " sure

until
    [ "$sure" = "y" ]
do
    read -p "Please enter the domain of the mirror 请输入镜像源的域名 [default=ftp.debian.org] : " domian
    mirror=${domian:-"ftp.debian.org"}
    sed -i "s?ftp.debian.orgS?$mirror?g" /etc/apt/sources.list
    printf "############################################################# \n"
    printf "The domain of the mirror is: $mirror \n"
    printf "你所选择的镜像源是：$mirror \n"
    printf "############################################################# \n"
    read -p "Are you sure? 你确定吗？(y/n) " sure
done

# apt改用https协议
if [ $stable -eq 1 ]; then

    # 备份sources.list文件
    mkdir -p /etc/apt.bak
    if [ ! -f "/etc/apt.bak/sources.list" ];then
        cp /etc/apt/sources.list /etc/apt.bak/sources.list
    fi

    # 更换源镜像
    echo "" | tee /etc/apt/sources.list
    echo "deb https://security.debian.org/debian-security stable-security main contrib non-free non-free-firmware" | tee -a /etc/apt/sources.list
    echo "deb https://$mirror/debian stable main" | tee -a /etc/apt/sources.list
    echo "deb https://$mirror/debian stable-updates main" | tee -a /etc/apt/sources.list
    echo "#deb https://$mirror/debian bookworm-backports main" | tee -a /etc/apt/sources.list

fi

# 准备创建新用户
cat << EOF

Create a New User to system. The default username is ec2-user.
创建新用户。默认用户名为 ec2-user

EOF

read -p "Enter username 输入新用户名 [default=ec2-user] : " user
username=${user:-"ec2-user"}

echo "
New user $username is being created.
正在创建新用户$username
"
sleep 5


# 提示上传公钥
sure="undefined"

if [ -s "/home/$username/.ssh/authorized_keys" ]; then
    echo "The public key is already installed. 公钥已存在。"
else
    echo "Your public key is usually generated by your device and stored on your device"
    echo "你的公钥通常由你的设备产生，并储存在你的设备上"
    read -p "Please enter your public key 请输入你的公钥 : " key
    touch /run/authorized_keys
    echo "$key" > /run/authorized_keys
    sha256=$(ssh-keygen -E sha256 -lf /run/authorized_keys)
    printf "############################################################# \n"
    printf "The key fingerprint is: $sha256 \n"
    printf "公钥的指纹是：$sha256 \n"
    printf "############################################################# \n"

    read -p "Are you sure? 你确定吗？(y/n) " sure
    until
        [ "$sure" = "y" ]
    do
        read -p "Please enter your public key 请输入你的公钥 : " key
        cat /dev/null > /run/authorized_keys
        echo "$key" > /run/authorized_keys
        sha256=$(ssh-keygen -E sha256 -lf /run/authorized_keys)
        printf "############################################################# \n"
        printf "The key fingerprint is: $sha256 \n"
        printf "公钥指纹是：$sha256 \n"
        printf "############################################################# \n"
        read -p "Are you sure? 你确定吗？(y/n) " sure
    done

fi

shred -u /run/authorized_keys
userdel -r $username


# 更新系统
apt-get clean
apt-get update

# 完整升级，更新软件包的版本，自动处理软件包之间的依赖关系变化，安装新的软件包，删除有冲突的软件包
#apt-get dist-upgrade -y

# 最小系统升级，避免删除大量你希望保留的软件包
#apt-get upgrade --without-new-pkgs -y

if [ $stable -eq 1 ]; then
    apt-get dist-upgrade -y
else
    apt-get upgrade -y
fi

# 自动更新
if [ $autoupdate -eq 1 ]; then

    apt-get install unattended-upgrades apt-listchanges -y
    sleep 2
    dpkg-reconfigure -plow unattended-upgrades

fi

# 安装少量软件
apt-get install auditd -y
apt-get install rsyslog -y
apt-get install htop -y
apt-get install nano -y
apt-get install sudo -y
apt-get install wget -y
apt-get install haveged -y
#apt-get install rng-tools5 -y
#apt-get install acl -y

# 安装mosh
if [ $mosh -eq 1 ]; then

    apt-get install mosh -y

fi

# 更换默认的ntp客户端
systemctl stop systemd-timesyncd 
systemctl disable systemd-timesyncd
systemctl stop chrony
systemctl disable chrony
apt purge systemd-timesyncd chrony -y
apt-get install ntpsec -y
systemctl enable ntpsec

# 开启NTS
tee /etc/ntpsec/ntp.conf << "EOF"
# /etc/ntpsec/ntp.conf, configuration for ntpd; see ntp.conf(5) for help

driftfile /var/lib/ntpsec/ntp.drift
leapfile /usr/share/zoneinfo/leap-seconds.list

# To enable Network Time Security support as a server, obtain a certificate
# (e.g. with Let's Encrypt), configure the paths below, and uncomment:
# nts cert CERT_FILE
# nts key KEY_FILE
# nts enable

# You must create /var/log/ntpsec (owned by ntpsec:ntpsec) to enable logging.
#statsdir /var/log/ntpsec/
#statistics loopstats peerstats clockstats
#filegen loopstats file loopstats type day enable
#filegen peerstats file peerstats type day enable
#filegen clockstats file clockstats type day enable

# This should be maxclock 7, but the pool entries count towards maxclock.
tos maxclock 11

# Comment this out if you have a refclock and want it to be able to discipline
# the clock by itself (e.g. if the system is not connected to the network).
tos minclock 4 minsane 3

# Specify one or more NTP servers.

# Public NTP servers supporting Network Time Security:
# server time.cloudflare.com nts
server ntppool1.time.nl nts
server ntppool2.time.nl nts
server time.cloudflare.com nts

# pool.ntp.org maps to about 1000 low-stratum NTP servers.  Your server will
# pick a different set every time it starts up.  Please consider joining the
# pool: <https://www.pool.ntp.org/join.html>
#pool 0.debian.pool.ntp.org iburst
#pool 1.debian.pool.ntp.org iburst
#pool 2.debian.pool.ntp.org iburst
#pool 3.debian.pool.ntp.org iburst

# Access control configuration; see /usr/share/doc/ntpsec-doc/html/accopt.html
# for details.
#
# Note that "restrict" applies to both servers and clients, so a configuration
# that might be intended to block requests from certain clients could also end
# up blocking replies from your own upstream servers.

# By default, exchange time with everybody, but don't allow configuration.
restrict default kod nomodify nopeer noquery limited

# Local users may interrogate the ntp server more closely.
restrict 127.0.0.1
restrict ::1

# Ntp server allows access only to local users
interface listen lo

EOF

mkdir -p /var/log/ntpsec

chown ntpsec:ntpsec /var/log/ntpsec

systemctl restart ntpsec

#ntpq -p

sleep 8

ntpq -c nts

sleep 6

# 开启DoT
apt purge systemd-resolved -y

apt install unbound -y

tee /etc/unbound/unbound.conf.d/dot-server.conf << "EOF"
server:
        interface: 127.0.0.1@53
        #interface: ::1@53
        hide-identity: yes
        hide-version: yes
        tls-cert-bundle: "/etc/ssl/certs/ca-certificates.crt"
        do-not-query-localhost: yes
        do-udp: yes
        do-tcp: yes
        do-ip4: yes
        do-ip6: no
        cache-min-ttl: 7200
        cache-max-ttl: 86400
        tcp-upstream: no
        prefetch: no
        minimal-responses: yes
        harden-glue: yes
        harden-dnssec-stripped: yes
        harden-below-nxdomain: yes
        harden-referral-path: no
        harden-algo-downgrade: no

forward-zone:
        name: "."
        forward-tls-upstream: yes
        #forward-tls-upstream: no
        forward-first: no
        #forward-addr: 146.255.56.98@853
        forward-addr: 185.253.5.254@853
        forward-addr: 199.58.83.33@53053
        #forward-addr: 2001:470:1c:76d::53@53053
        #forward-addr: 2a01:4f8:c0c:83ed::1@853
        #forward-addr: 2a0f:fc81::ffff@853
EOF

systemctl enable unbound

systemctl restart unbound

touch /etc/resolv.conf

echo "nameserver 127.0.0.1" | tee /etc/resolv.conf

if [ $stable -eq 1 ]; then

# 设置密码安全策略
apt-get install libpam-pwquality -y

# 密码至少有14位
# 修改 /etc/pam.d/common-password
mkdir -p /etc/pam.d.bak/
if [ ! -f "/etc/pam.d.bak/common-password" ];then
    cp /etc/pam.d/common-password /etc/pam.d.bak/common-password
fi

tee /etc/pam.d/common-password << "EOF"
# here are the per-package modules (the "Primary" block)
password    requisite           pam_pwquality.so retry=3 minlen=14
password    [success=1 default=ignore]  pam_unix.so obscure use_authtok try_first_pass yescrypt
# here's the fallback if no module succeeds
password    requisite           pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
password    required            pam_permit.so
# end of pam-auth-update config
EOF

# 设置密码有效期。每360天换一次密码，提前60天提醒密码即将到期
# 修改/etc/login.defs
if [ ! -f "/etc/login.defs.bak" ];then
    cp /etc/login.defs /etc/login.defs.bak
fi
DAYS=$(cat /etc/login.defs | grep "^PASS_MAX_DAYS")
AGE=$(cat /etc/login.defs | grep "^PASS_WARN_AGE")
sed -i "s#$DAYS#PASS_MAX_DAYS 360#g" /etc/login.defs
sed -i "s#$AGE#PASS_WARN_AGE 60#g" /etc/login.defs

# 减慢输入密码的速度
# 修改/etc/pam.d/login
mkdir -p /etc/pam.d.bak/
if [ ! -f "/etc/pam.d.bak/login" ];then
    cp /etc/pam.d/login /etc/pam.d.bak/login
fi
sed -i "s?auth       optional   pam_faildelay.so  delay=3000000?auth       optional   pam_faildelay.so  delay=6000000?g" /etc/pam.d/login

fi

if [ $stable -eq 1 ] && [ $faillock -eq 1 ]; then

# 登录失败次数过多自动锁定账户
# 修改/etc/pam.d/common-auth
mkdir -p /etc/pam.d.bak
if [ ! -f "/etc/pam.d.bak/common-auth" ];then
    cp /etc/pam.d/common-auth /etc/pam.d.bak/common-auth
fi
tee /etc/pam.d/common-auth << "EOF"
auth    [success=1 default=bad] pam_unix.so
auth    [default=die]           pam_faillock.so authfail
auth    sufficient              pam_faillock.so authsucc
auth    required                pam_deny.so
auth    required                pam_permit.so
EOF

# 60分钟内输错30次密码，自动锁定账号60分钟
# 修改 /etc/security/faillock.conf
mkdir -p /etc/security.bak/
if [ ! -f "/etc/security.bak/faillock.conf" ];then
    cp /etc/security/faillock.conf /etc/security.bak/faillock.conf
fi
tee /etc/security/faillock.conf << "EOF"
silent
deny = 30
fail_interval = 3600
unlock_time = 3600
even_deny_root
root_unlock_time = 3600
audit
EOF

fi

# 显示熵的大小
entropy="$(cat /proc/sys/kernel/random/entropy_avail)"
echo "
the available entropy is $entropy
目前熵池的大小是$entropy
" 

sleep 5

# 开始创建新用户

# 为新用户产生密码
MakePassword

# 开始检测密码的强度
CheckPassword

# 为新用户设置密码
newuser_password=$password

useradd --shell /bin/bash --create-home --password $EncryptedPassword $username

if [ `echo $?` = "0" ];then

    echo "
    user $username has been added to system!
    用户$username创建成功！
    "

fi

sleep 2

# 为新用户赋予 sudo 权限
usermod -a -G sudo $username

echo "
All sudo users in your system is here:
以下用户具有sudo权限：
"
getent group sudo

sleep 10

# 获取新用户的home目录的路径
user_path=$(getent passwd $username | cut -f6 -d:)

# 修改SSH设置
sed -i 's/PermitRootLogin yes/#PermitRootLogin yes/' /etc/ssh/sshd_config

sed -i 's/PasswordAuthentication yes/#PasswordAuthentication no/' /etc/ssh/sshd_config

sed -i 's/PermitEmptyPasswords no/#PermitEmptyPasswords no/' /etc/ssh/sshd_config

sed -i 's/KbdInteractiveAuthentication no/#KbdInteractiveAuthentication no/' /etc/ssh/sshd_config

sed -i 's/ChallengeResponseAuthentication no/#ChallengeResponseAuthentication no/' /etc/ssh/sshd_config

sed -i 's/UsePAM yes/#UsePAM yes/' /etc/ssh/sshd_config

sed -i 's/X11Forwarding yes/#X11Forwarding yes/' /etc/ssh/sshd_config

sed -i 's/PrintMotd no/#PrintMotd no/' /etc/ssh/sshd_config

sed -i 's/AcceptEnv LANG LC_*/#AcceptEnv LANG LC_*/' /etc/ssh/sshd_config

sed -i 's/Subsystem/#Subsystem/' /etc/ssh/sshd_config

PrivateIP=`ip a | grep inet | grep -v inet6 | grep -v '127.0.0.1' | awk '{print $2}' | awk -F / '{print$1}'`

tee /etc/ssh/sshd_config.d/sshd.conf << EOF
Port $openssh_port
#AddressFamily any
AddressFamily inet
ListenAddress 0.0.0.0
#ListenAddress ::
#ListenAddress $private_ip
Protocol 2

#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
#RekeyLimit default none

# Logging
#SyslogFacility AUTH
LogLevel INFO

# Authentication:

LoginGraceTime 2m
#PermitRootLogin prohibit-password
#PermitRootLogin yes
PermitRootLogin no
StrictModes yes
#MaxAuthTries 6
MaxAuthTries 2
#MaxSessions 10
MaxSessions 6

#PubkeyAuthentication no
PubkeyAuthentication yes

# Expect .ssh/authorized_keys2 to be disregarded by default in future.
#AuthorizedKeysFile .ssh/authorized_keys .ssh/authorized_keys2
AuthorizedKeysFile $user_path/.ssh/authorized_keys

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
HostbasedAuthentication no
# Change to yes if you dont trust ~/.ssh/known_hosts for 
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Dont read the user's ~/.rhosts and ~/.shosts files
IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
#PasswordAuthentication yes
PasswordAuthentication no
PermitEmptyPasswords no

# Change to yes to enable challenge-response passwords (beware issues with
# some PAM modules and threads)
ChallengeResponseAuthentication no

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no

# GSSAPI options
GSSAPIAuthentication no
#GSSAPICleanupCredentials yes
GSSAPICleanupCredentials no
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
UsePAM yes

#AllowAgentForwarding yes
AllowAgentForwarding no
#AllowTcpForwarding yes
AllowTcpForwarding no
#GatewayPorts no
#X11Forwarding yes
X11Forwarding no
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes
PrintMotd no
#PrintLastLog yes
TCPKeepAlive yes
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
ClientAliveInterval 3600
ClientAliveCountMax 1
UseDNS no
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# Allow client to pass locale environment variables
#AcceptEnv LANG LC_*

# override default of no subsystems
#Subsystem sftp /usr/lib/openssh/sftp-server
Subsystem sftp /usr/lib/openssh/sftp-server -f AUTHPRIV -l INFO

AllowUsers $username

KexAlgorithms sntrup761x25519-sha512@openssh.com,curve25519-sha256,curve25519-sha256@libssh.org,gss-curve25519-sha256-,diffie-hellman-group16-sha512,gss-group16-sha512-,diffie-hellman-group18-sha512,diffie-hellman-group-exchange-sha256

Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr

MACs hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,umac-128-etm@openssh.com
EOF

# 屏蔽ECDSA
#rm /etc/ssh/ssh_host_ecdsa_key*
#rm /etc/ssh/ssh_host_key*
#ln -s /etc/ssh/ssh_host_ecdsa_key /etc/ssh/ssh_host_ecdsa_key
#ln -s /etc/ssh/ssh_host_key /etc/ssh/ssh_host_key

# 停用长度过短的moduli
awk '$5 >= 3071' /etc/ssh/moduli > /etc/ssh/moduli.tmp
mv -f /etc/ssh/moduli.tmp /etc/ssh/moduli

systemctl restart sshd

# 允许OpenSSH服务端使用IPv6
if [ $openssh_ipv6 -eq 1 ]; then

    sed -i 's/#AddressFamily any/AddressFamily any/' /etc/ssh/sshd_config.d/sshd.conf
    sed -i 's/AddressFamily inet/#AddressFamily inet/' /etc/ssh/sshd_config.d/sshd.conf
    sed -i 's/#ListenAddress ::/ListenAddress ::/' /etc/ssh/sshd_config.d/sshd.conf
    systemctl restart sshd

fi

# OpenSSH服务端程序只和私有网络通信
if [ $vpc -eq 1 ]; then

    sed -i 's/ListenAddress 0.0.0.0/#ListenAddress 0.0.0.0/' /etc/ssh/sshd_config.d/sshd.conf
    sed -i 's/ListenAddress ::/#ListenAddress ::/' /etc/ssh/sshd_config.d/sshd.conf
    sed -i 's/#ListenAddress $PrivateIP/ListenAddress $PrivateIP/' /etc/ssh/sshd_config.d/sshd.conf
    systemctl restart sshd

fi

# 输入密码时显示星号（仅限sudo命令）
if [ ! -f "/etc/sudoers.bak" ];then
    cp /etc/sudoers /etc/sudoers.bak
fi
chattr -i /etc/sudoers
chmod 600 /etc/sudoers
sed -i "s/env_reset/env_reset,pwfeedback,timestamp_timeout=30/g" /etc/sudoers
chmod 440 /etc/sudoers
chattr +i /etc/sudoers

# 准备安装公钥
if [ ! -d "$user_path/.ssh" ]; then
    mkdir -p $user_path/.ssh 
fi

if [ ! -f "$user_path/.ssh/authorized_keys" ]; then
    touch $user_path/.ssh/authorized_keys
fi

chown -R $username:$username $user_path/.ssh/
chmod 700 $user_path/.ssh/
chmod 600 $user_path/.ssh/authorized_keys

#getfacl $user_path/.ssh/authorized_keys
ls -l $user_path/.ssh/authorized_keys

sleep 2


# 自动安装公钥
if [ -s "$user_path/.ssh/authorized_keys" ]; then
    echo "The public key is already installed. 公钥已存在。"
else
    echo "$key" > $user_path/.ssh/authorized_keys
    publickey=$(cat $user_path/.ssh/authorized_keys)
    fingerprint=$(ssh-keygen -E sha256 -lf $user_path/.ssh/authorized_keys)
    printf "############################################################# \n"
    printf "Your public key has been saved in $user_path/.ssh/authorized_keys \n"
    printf "The key fingerprint is: $fingerprint \n"
    printf "你的公钥已经保存在 $user_path/.ssh/authorized_keys \n"
    printf "公钥指纹是：$fingerprint \n"
    printf "############################################################# \n"
    sleep 5
    systemctl restart sshd
fi

# 阻止其他用户写入新的公钥
chmod 400 $user_path/.ssh/authorized_keys
chattr -R +i $user_path/.ssh/


# 锁定多余的用户，删除多余的组
userdel -r systemd-timesync
userdel -r _chrony
passwd -l adm
passwd -l lp
passwd -l sync
passwd -l shutdown
passwd -l halt
passwd -l news
passwd -l uucp
passwd -l operator
passwd -l games
passwd -l gopher
passwd -l ftp
passwd -l irc
passwd -l backup
passwd -l list
groupdel dip
groupdel pppusers
groupdel slipusers
groupdel audio
groupdel video
groupdel scanner
groupdel ftp
groupdel floppy
groupdel fax
groupdel bluetooth

# 显示现有用户
echo "
The following users exist in the system:
现在系统里存在以下用户：
"
cut -d: -f1 /etc/passwd

sleep 10

echo "
These users exist in special groups:
以下用户位于特殊的组里：
"
getent group disk
getent group floppy
getent group kmem 
getent group shadow
getent group sudo
getent group staff
getent group wheel

sleep 5

# 锁定口令文件
#chattr +i /etc/passwd
#chattr +i /etc/shadow
#chattr +i /etc/group
#chattr +i /etc/gshadow

# 解锁口令文件
#chattr -i /etc/passwd
#chattr -i /etc/shadow
#chattr -i /etc/group
#chattr -i /etc/gshadow

# 为root用户产生新密码
MakePassword

# 开始检测密码的强度
CheckPassword

# 为root用户设置新的密码
usermod -s /bin/bash -p $EncryptedPassword root

root_password=$password

echo "
root's password has been changed
root用户的密码已修改
"

# 锁定root用户
passwd -l root

# 阻止其他用户写入新的公钥
mkdir -p /root/.ssh/
chmod 700 /root/.ssh/
rm -rf /root/.ssh/authorized_keys
touch /root/.ssh/authorized_keys
chmod 400 /root/.ssh/authorized_keys
chattr -R +i /root/.ssh/

# 卸载x11
apt purge 'libx11-*' -y

# 清理系统
apt-get autoremove -y

# 修改欢迎信息
cat /dev/null > /etc/motd

# 修改网卡名
sed -i 's/GRUB_CMDLINE_LINUX=""/GRUB_CMDLINE_LINUX="net.ifnames=0 biosdevname=0"/g' /etc/default/grub

sed -i 's/"console=tty0 console=ttyS0,115200 earlyprintk=ttyS0,115200 consoleblank=0"/"console=tty0 console=ttyS0,115200 earlyprintk=ttyS0,115200 consoleblank=0 net.ifnames=0 biosdevname=0"/g' /etc/default/grub

update-grub

#
RSA_SHA256=$(ssh-keygen -E sha256 -lf /etc/ssh/ssh_host_rsa_key.pub)
RSA_MD5=$(ssh-keygen -E md5 -lf /etc/ssh/ssh_host_rsa_key.pub)
ED25519_SAH256=$(ssh-keygen -E sha256 -lf /etc/ssh/ssh_host_ed25519_key.pub)
ED25519_MD5=$(ssh-keygen -E md5 -lf /etc/ssh/ssh_host_ed25519_key.pub)

# 文字提示
echo "
#############################################################
root's password is $root_password
root的密码是 $root_password
#############################################################
-----BEGIN SSH HOST KEY FINGERPRINTS-----
$RSA_SHA256
$RSA_MD5
$ED25519_SAH256
$ED25519_MD5
-----END SSH HOST KEY FINGERPRINTS-----
#############################################################
$username's password is $newuser_password
$username的密码是 $newuser_password
#############################################################
"
