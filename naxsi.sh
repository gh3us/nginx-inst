#!/bin/sh

# 选择安装 WAF
cat << EOF

naxsi is an open-source, high performance, low rules maintenance waf for nginx

EOF

read -p "install naxsi ? (Y/n): " input
case $input in
  n) 
waf=0 
waf_mod_1=''
;;
*) 
waf=1 
waf_mod_1='--add-module=../naxsi/naxsi_src'
;;
esac 

# 安装依赖
if [ -e "/usr/bin/yum" ]; then
  yum update -y
  yum install libpcre3-dev libssl-dev perl curl zlib1g-dev git gcc make build-essential logrotate cron -y
fi
if [ -e "/usr/bin/apt-get" ]; then
  apt-get update -y
  apt-get install libpcre3-dev libssl-dev perl curl zlib1g-dev git gcc make build-essential logrotate cron -y
fi

# 准备
rm -rf /usr/src/
mkdir -p /usr/src/
mkdir -p /var/log/nginx/
useradd -s /sbin/nologin -M www-data

# 下载 openssl
# 开启 https
#cd /usr/src
#wget https://github.com/openssl/openssl/archive/OpenSSL_1_1_1.tar.gz 
#tar xzvf OpenSSL_1_1_1.tar.gz
#mv openssl-OpenSSL_1_1_1 openssl
cd /usr/src/
openssl_v='1.1.1k'
wget https://www.openssl.org/source/openssl-${openssl_v}.tar.gz
tar -zxvf openssl-${openssl_v}.tar.gz
mv openssl-${openssl_v} openssl

# 下载 nginx
cd /usr/src/
nginx_v='1.18.0'
wget https://nginx.org/download/nginx-${nginx_v}.tar.gz
tar zxvf ./nginx-${nginx_v}.tar.gz 
mv nginx-${nginx_v} nginx

# 下载 zlib
# 开启 gzip 压缩
cd /usr/src/
git clone https://github.com/cloudflare/zlib.git zlib
cd zlib
make -f Makefile.in distclean

# 下载 pcre
# 用于正则
cd /usr/src/
#wget https://ftp.pcre.org/pub/pcre/pcre-8.43.tar.gz
#tar zxf ./pcre-8.43.tar.gz
#mv pcre-8.43 pcre
wget https://ftp.pcre.org/pub/pcre/pcre-8.44.tar.gz 
tar -zxvf pcre-8.44.tar.gz
mv pcre-8.44 pcre

# 安装 GeoIP2 依赖
cd /usr/src/
wget https://github.com/maxmind/libmaxminddb/releases/download/1.4.2/libmaxminddb-1.4.2.tar.gz
tar -zxvf libmaxminddb-1.4.2.tar.gz
cd libmaxminddb-1.4.2
./configure && make && make install
echo /usr/local/lib  >> /etc/ld.so.conf.d/local.conf 
ldconfig

# 下载 GeoIP2 
cd /usr/src/
git clone https://github.com/ar414-com/nginx-geoip2

if [ $waf -eq 1 ]; then

# 下载 naxsi 防火墙
cd /usr/src/
#wget https://github.com/nbs-system/naxsi/archive/0.56.tar.gz
#tar xzvf 0.56.tar.gz
#mv naxsi-0.56 naxsi
wget https://github.com/nbs-system/naxsi/archive/1.3.tar.gz
tar xzvf 1.3.tar.gz
mv naxsi-1.3 naxsi

fi

# 关闭 nginx 的 debug 模式
sed -i 's@CFLAGS="$CFLAGS -g"@#CFLAGS="$CFLAGS -g"@' /usr/src/nginx/auto/cc/gcc

# 编译安装 nginx
cd /usr/src/nginx
./configure \
--user=www-data --group=www-data \
--prefix=/usr/local/nginx \
--sbin-path=/usr/sbin/nginx \
--with-http_v2_module --with-http_v2_hpack_enc \
--with-http_realip_module --with-http_slice_module \
--with-openssl=../openssl --with-http_ssl_module \
--with-pcre=../pcre --with-pcre-jit \
--with-zlib=../zlib --with-http_gzip_static_module \
--add-module=../nginx-geoip2/ngx_http_geoip2_module \
${waf_mod_1} 


make -j2 && make install


# 创建 naxsi 规则文件
cd /usr/local/nginx/conf/
rm -rf /usr/local/nginx/conf/waf
mkdir -p /usr/local/nginx/conf/waf
chown www-data:www-data /usr/local/nginx/conf/waf
cp /usr/src/naxsi/naxsi_config/naxsi_core.rules /usr/local/nginx/conf/waf/naxsi_core.rules

cat > "/usr/local/nginx/conf/waf/naxsi.rules" << "EOF"
LearningMode; #启用学习模式，即拦截请求后不拒绝访问，只将触发规则的请求写入日志
SecRulesEnabled; #
#SecRulesDisabled;
DeniedUrl "/RequestDenied"; #拒绝访问时展示的页面
## 检查规则
CheckRule "$SQL >= 8" BLOCK;
CheckRule "$RFI >= 8" BLOCK;
CheckRule "$TRAVERSAL >= 4" BLOCK;
CheckRule "$EVADE >= 4" BLOCK;
CheckRule "$XSS >= 8" BLOCK;
EOF

# 创建 nginx 全局配置
cat > "/usr/local/nginx/conf/nginx.conf" << OOO
user www-data www-data;
pid /var/run/nginx.pid;
worker_processes auto;
worker_rlimit_nofile 65535;

events {
  use epoll;
  multi_accept on;
  worker_connections 65535;
}

http {
  charset utf-8;
  sendfile on;
  tcp_nopush on;
  tcp_nodelay on;
  server_tokens off;
  log_not_found off;
  types_hash_max_size 2048;
  client_max_body_size 16M;

  # MIME
  include mime.types;
  default_type application/octet-stream;

  # Logging
  access_log /var/log/nginx/access.log;
  error_log /var/log/nginx/error.log warn;

  # Gzip
  gzip on;
  gzip_vary on;
  gzip_proxied any;
  gzip_comp_level 6;
  gzip_types text/plain text/css text/xml application/json application/javascript application/xml+rss application/atom+xml image/svg+xml;
  gzip_disable "MSIE [1-6]\.(?!.*SV1)";

  include vhost/*.conf;
  include /usr/local/nginx/conf/waf/naxsi_core.rules; # 引用核心规则
}
OOO

# 创建 nginx 服务进程
mkdir -p /usr/lib/systemd/system/ 
cat > /usr/lib/systemd/system/nginx.service << "EOF"
[Unit]
Description=nginx - high performance web server
After=network.target

[Service]
Type=forking
PIDFile=/var/run/nginx.pid
ExecStartPost=/bin/sleep 0.1
ExecStartPre=/usr/sbin/nginx -t -c /usr/local/nginx/conf/nginx.conf
ExecStart=/usr/sbin/nginx -c /usr/local/nginx/conf/nginx.conf
ExecReload=/usr/sbin/nginx -s reload
ExecStop=/usr/sbin/nginx -s stop

[Install]
WantedBy=multi-user.target
EOF




# 配置默认站点
mkdir -p /wwwroot/
cp -r /usr/local/nginx/html /wwwroot/default
mkdir -p /usr/local/nginx/conf/vhost/
mkdir -p /usr/local/nginx/conf/ssl/
cat > "/usr/local/nginx/conf/vhost/default.conf" << "EOF"
server {
  listen 80;
  root /wwwroot/default;
  location / {
    include /usr/local/nginx/conf/waf/naxsi.rules; # 引用子规则
    index  index.html;
  }
}
EOF

# 配置站点目录权限
chown -R www-data:www-data /wwwroot/
find /wwwroot/ -type d -exec chmod 755 {} \;
find /wwwroot/ -type f -exec chmod 644 {} \;

# 开启 nginx 服务进程
systemctl unmask nginx.service
systemctl daemon-reload
systemctl enable nginx
systemctl start nginx
