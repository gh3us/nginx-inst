# nginx-install

​	

安装git

```shell
sudo apt update && apt install git -y
```

​	

快速初始化环境 

```shell
sudo -u nobody git clone https://gh3us@bitbucket.org/gh3us/nginx-inst.git "/tmp/ngx" && cd /tmp/ngx && chmod +x *.sh && ./init.sh
```

​	

快速编译Nginx

```shell
sudo -u nobody git clone https://gh3us@bitbucket.org/gh3us/nginx-inst.git "/tmp/ngx" && cd /tmp/ngx && chmod +x *.sh && ./nginx.sh
```

​	

快速编译OpenRestry

```shell
sudo -u nobody git clone https://gh3us@bitbucket.org/gh3us/nginx-inst.git "/tmp/ngx" && cd /tmp/ngx && chmod +x *.sh && ./openresty.sh
```

​	

快速搭建Lua环境

```shell
sudo -u nobody git clone https://gh3us@bitbucket.org/gh3us/nginx-inst.git "/tmp/ngx" && cd /tmp/ngx && chmod +x *.sh && ./lua.sh
```

​	

快速安装Naxsi

```shell
sudo -u nobody git clone https://gh3us@bitbucket.org/gh3us/nginx-inst.git "/tmp/ngx" && cd /tmp/ngx && chmod +x *.sh && ./naxsi.sh
```

​	


- 支持TLS1.3 
- 用LibreSSL代替OpenSSL 
- 支持HTTP 2.0
- 支持HTTP 3.0
- 自动创建Nginx服务
- Nginx日志自动分割
