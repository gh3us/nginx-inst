#!/bin/sh

number=$(cat /etc/ssh/sshd_config | grep -oE "^Port [0-9]*$" | grep -oE "[0-9]*")
username=$(cat /etc/ssh/sshd_config | grep "^AllowUsers" | sed 's/AllowUsers//g' | sed 's/[[:space:]]//g')

# 更换源镜像
cat /dev/null > /etc/apt/sources.list
echo 'deb http://http.us.debian.org/debian bullseye main ' | tee -a /etc/apt/sources.list
echo 'deb http://http.us.debian.org/debian bullseye-updates main ' | tee -a /etc/apt/sources.list
sleep 2
apt update 

# 安装依赖
apt-get install --only-upgrade openssl -y
apt-get install libssl-dev libpcre3-dev zlib1g-dev -y
apt-get install --only-upgrade apt -y
apt-get install gcc make build-essential -y
apt-get install --only-upgrade logrotate -y
apt-get install --only-upgrade cron -y

# 安装 nginx-full
apt-get install nginx -y


# 修复 SSH 设置
cat /dev/null > /etc/ssh/sshd_config
cat > "/etc/ssh/sshd_config" << OOO
Port $number
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::
Protocol 2

HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
#RekeyLimit default none

# Logging
#SyslogFacility AUTH
LogLevel INFO

# Authentication:

#LoginGraceTime 2m
LoginGraceTime 1m
#PermitRootLogin prohibit-password
PermitRootLogin no
StrictModes yes
MaxAuthTries 6
MaxAuthTries 1
#MaxSessions 10
MaxSessions 6

PubkeyAuthentication yes

# Expect .ssh/authorized_keys2 to be disregarded by default in future.
#AuthorizedKeysFile	.ssh/authorized_keys .ssh/authorized_keys2
AuthorizedKeysFile $user_path/.ssh/authorized_keys

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
#PasswordAuthentication yes
PasswordAuthentication no
PermitEmptyPasswords no

# Change to yes to enable challenge-response passwords (beware issues with
# some PAM modules and threads)
ChallengeResponseAuthentication no

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no

# GSSAPI options
GSSAPIAuthentication no
#GSSAPICleanupCredentials yes
GSSAPICleanupCredentials no
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
UsePAM yes

#AllowAgentForwarding yes
AllowAgentForwarding no
#AllowTcpForwarding yes
AllowTcpForwarding no
#GatewayPorts no
#X11Forwarding yes
X11Forwarding no
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes
#PrintMotd no
PrintMotd yes
#PrintLastLog yes
TCPKeepAlive yes
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
ClientAliveInterval 600
ClientAliveCountMax 3
UseDNS no
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# Allow client to pass locale environment variables
AcceptEnv LANG LC_*

# override default of no subsystems
Subsystem	sftp	/usr/lib/openssh/sftp-server

# Example of overriding settings on a per-user basis
#Match User anoncvs
#	X11Forwarding no
#	AllowTcpForwarding no
#	PermitTTY no
#	ForceCommand cvs server

AllowUsers $username

OOO

#service sshd restart
systemctl restart sshd

# 安装 modsecurity 库
apt-get install libmodsecurity-dev -y

# 安装 Lua 模块
apt-get install libnginx-mod-http-lua -y

# 安装 headers-more-nginx-module 模块
apt-get install libnginx-mod-http-headers-more-filter -y

# 安装 geoip2 模块
apt-get install libnginx-mod-http-geoip2 -y


# 下载 nginx
cd /usr/src/
version='1.18.0'
wget https://deb.debian.org/debian/pool/main/n/nginx/nginx_${version}.orig.tar.gz
tar zxvf nginx_${version}.orig.tar.gz
mv nginx-${version} nginx

# 下载 ModSecurity-nginx 模块
cd /usr/src/
git clone --depth 1 https://github.com/SpiderLabs/ModSecurity-nginx.git

# 编译 ModSecurity-nginx 模块
cd nginx
./configure --with-compat --add-dynamic-module=/usr/src/ModSecurity-nginx
make modules

# 复制 ModSecurity-nginx 模块到标准目录
cd objs
cp ngx_http_modsecurity_module.so /usr/lib/nginx/modules

# 加载 ModSecurity-nginx 模块
cd 
cat /dev/null > /usr/share/nginx/modules-available/mod-http-modsecurity.conf
cat > "/usr/share/nginx/modules-available/mod-http-modsecurity.conf" << OOO
load_module modules/ngx_http_modsecurity_module.so;
OOO
ln -s /usr/share/nginx/modules-available/mod-http-modsecurity.conf /etc/nginx/modules-enabled/50-mod-http-modsecurity.conf 
ldconfig

# ModSecurity 初始化设置
cd /usr/src/

mkdir -p /etc/nginx/waf/
wget https://example.com/conf/bin/modsecurity.conf
cp -p /usr/src/modsecurity.conf /etc/nginx/waf/modsecurity.conf

cd /usr/src/
wget https://github.com/SpiderLabs/ModSecurity/releases/download/v3.0.4/modsecurity-v3.0.4.tar.gz
tar -xvf modsecurity-v3.0.4.tar.gz
cp -p /usr/src/modsecurity-v3.0.4/unicode.mapping /etc/nginx/waf/unicode.mapping

cd /usr/src/
wget https://example.com/conf/bin/owasp_waf.conf
cp -p /usr/src/owasp_waf.conf /etc/nginx/waf/owasp_waf.conf


# 下载 OWASP 核心规则集（CRS）
cd /usr/src/
wget https://github.com/coreruleset/coreruleset/archive/v3.3.0.tar.gz -O CRS_v3.3.0.tar.gz
tar -xvf /usr/src/CRS_v3.3.0.tar.gz
mv coreruleset-3.3.0 crs
cp -r /usr/src/crs /etc/nginx/waf/crs
cp -p /usr/src/crs/crs-setup.conf.example /etc/nginx/waf/crs/crs-setup.conf
sed -i 's?SecDefaultAction \"phase:1,log,auditlog,pass\"?# SecDefaultAction \"phase:1,log,auditlog,pass\"?g' /etc/nginx/waf/crs/crs-setup.conf
sed -i 's?SecDefaultAction \"phase:2,log,auditlog,pass\"?# SecDefaultAction \"phase:2,log,auditlog,pass\"?g' /etc/nginx/waf/crs/crs-setup.conf
sed -i 's?# SecDefaultAction \"phase:1,log,auditlog,deny,status:403\"?SecDefaultAction "phase:1,log,auditlog,deny,status:403"?g' /etc/nginx/waf/crs/crs-setup.conf
sed -i 's?# SecDefaultAction \"phase:2,log,auditlog,deny,status:403\"?SecDefaultAction "phase:2,log,auditlog,deny,status:403"?g' /etc/nginx/waf/crs/crs-setup.conf


# 创建 nginx 全局配置文件
cd /usr/src/
wget https://example.com/conf/bin/nginx.conf
rm -f /etc/nginx/nginx.conf
cp -p /usr/src/nginx.conf /etc/nginx/nginx.conf 

# 创建目录
cd
rm -rf /etc/nginx/ssl/
mkdir -p /etc/nginx/ssl/
chown -R root:root /etc/nginx/ssl/
find /etc/nginx/ssl/ -type d -exec chmod 755 {} \;

# 创建虚拟主机
cd
rm -rf /etc/nginx/conf.d/
mkdir -p /etc/nginx/conf.d/
cd /usr/src/
wget https://example.com/conf/bin/test.conf
cp -p /usr/src/test.conf /etc/nginx/conf.d/test.conf 

# 重启 nginx 服务进程
systemctl unmask nginx.service
systemctl daemon-reload
systemctl enable nginx
systemctl restart nginx

# 更换源镜像
#sed -i '$d' /etc/apt/sources.list

# 清理无用软件包
apt autoremove -y

# 查看 nginx 运行状态
systemctl status nginx
