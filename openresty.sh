#!/bin/sh

# 安装依赖
apt update -y
apt install apt-utils git curl logrotate cron -y
apt install flex bison libgeoip1 dh-autoreconf libcurl4-gnutls-dev libpcre++-dev -y
apt install libreadline-dev liblmdb-dev perl libperl-dev -y

apt install gcc g++ make build-essential autoconf automake libtool gettext -y
apt install pkg-config libpcre3 libpcre3-dev libxml2 libxml2-dev libcurl4 libgeoip-dev -y
apt install libyajl-dev doxygen -y

# 准备
rm -rf /usr/src/
mkdir -p /usr/src/
mkdir -p /var/log/nginx/
groupadd nginx
useradd -s /sbin/nologin -M -g nginx nginx

# 下载安装 openssl
# 开启 https
cd /usr/src/
openssl_v='1.1.1k'
wget https://www.openssl.org/source/openssl-${openssl_v}.tar.gz
tar -zxvf openssl-${openssl_v}.tar.gz
mv openssl-${openssl_v} openssl


# 下载安装 zlib 库
# 开启 gzip 压缩
cd /usr/src/
git clone https://github.com/cloudflare/zlib.git zlib
cd zlib
make -f Makefile.in distclean

# 下载安装 pcre 库
# 用于正则表达式
cd /usr/src/
wget https://ftp.pcre.org/pub/pcre/pcre-8.44.tar.gz 
tar -zxvf pcre-8.44.tar.gz
mv pcre-8.44 pcre

# 安装 libmaxminddb 支持库
# IP查询
#cd /usr/src/
#git clone --recursive https://github.com/maxmind/libmaxminddb
#cd libmaxminddb
#./bootstrap
#./configure && make && make check 
#make install
#ldconfig

# 下载 GeoIP2 数据和 ngx_http_geoip2_module 模块
# IP查询
#cd /usr/src/
#git clone https://github.com/ar414-com/nginx-geoip2
#cd nginx-geoip2
#tar -zxvf GeoLite2-City_20200519.tar.gz
#mv /usr/src/nginx-geoip2/GeoLite2-City_20200519/GeoLite2-City.mmdb /usr/local
#tar -zxvf GeoLite2-Country_20200519.tar.gz
#mv /usr/src/nginx-geoip2/GeoLite2-Country_20200519/GeoLite2-Country.mmdb /usr/local

# 安装 modsecurity 库
cd /usr/src/
git clone https://github.com/SpiderLabs/ModSecurity
cd ModSecurity/
git checkout v3/master
git submodule init
git submodule update
./build.sh
./configure
make
make install

# 半自动编译
rm -f /usr/sbin/nginx
#cd
cd ~/compile-nginx/conf
chmod +x openresty-build.sh 
./openresty-build.sh


# 自动编译
#sleep 5
#cd /usr/src/
#wget https://bitbucket.org/gh3us/compile-nginx/downloads/openresty_build.sh
#chmod +x openresty_build.sh 
#./openresty_build.sh
